
## Dependencies

	apt install \
	cmake pkg-config \
	libavformat-dev libavutil-dev libavcodec-dev libswresample-dev \
	libprotobuf-c-dev

## Building

	# Generate headers from options proto
	protoc-c --c_out=. ffchunk_options.proto
	mkdir build && cd build && cmake .. && make

This will put the binaries in your `build` directory. If you'd like to install them system-wide, use `make install`.

Regenerating the headers from the options proto is also required after editing the proto descriptor file, e.g. to add new options.

## Passing options manually

Options are passed to the binary in Protobuf format to remove the burden of parsing arguments in C. To run manually, create a file `test_options.msg` with your proto message in text format and pass it to the binary like so (assuming you are in the ffchunk source directory and the binary is in your `PATH`):

	cat test_options.msg | protoc-c --encode=FFChunkOptions ffchunk_options.proto | ffchunk_transmux
