#ifndef FFCHUNK_COMMON_H
#define FFCHUNK_COMMON_H

#include <libavutil/timestamp.h>
#include <libavutil/opt.h>

#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>

#define MAX_OPTIONS_MSG_SIZE 1024 * 100

#include "util.h"
#include "segment.h"
#include "ffchunk_options.pb-c.h"

typedef struct {
    char* input_file;
    int input_stream_index;

    char* output_dir;

    AVFormatContext* input_context;
    AVStream* input_stream;
    AVFormatContext* output_context;
    AVStream* output_stream;

    int current_segment_index;
    // Whether the next frame should be a keyframe, usually after a segment split point.
    int keyframe_required;

    int first_packet;
    int64_t actual_start_dts;

    // For implementation-specific context
    void* priv;
} Context;

int run(int argc, char **argv,
        int (*init)(Context* ctx),
        int (*setup_input_stream)(Context* ctx),
        int (*setup_output_stream)(Context* ctx),
        int (*process_packet)(Context* ctx, AVPacket* pkt)) {
    int ret;

    av_register_all();
    avcodec_register_all();

    av_log_set_level(AV_LOG_DEBUG);

    uint8_t options_msg_buf[MAX_OPTIONS_MSG_SIZE];
    size_t options_msg_len;
    // Read options from file if given, else from stdin
    if (argc > 1) {
        options_msg_len = fread(&options_msg_buf, 1, MAX_OPTIONS_MSG_SIZE, fopen(argv[1], "r"));
    } else {
        options_msg_len = fread(&options_msg_buf, 1, MAX_OPTIONS_MSG_SIZE, stdin);
    }
    FFChunkOptions* options_msg = ffchunk_options__unpack(NULL, options_msg_len, options_msg_buf);

    Context *ctx = malloc(sizeof(Context));
    ctx->input_file = options_msg->input_file;
    ctx->output_dir = options_msg->output_dir;
    ctx->input_stream_index = options_msg->stream_index;

    if (init != NULL) {
        ret = init(ctx);
        if (ret) {
            av_log(NULL, AV_LOG_FATAL, "init failed");
            exit(ret);
        }
    }

    ctx->input_context = NULL;
    if(avformat_open_input(&ctx->input_context, ctx->input_file, NULL, NULL)) {
        av_log(ctx->input_context, AV_LOG_FATAL, "can't open input file '%s'", ctx->input_file);
        exit(EXIT_FAILURE);
    }
    if (avformat_find_stream_info(ctx->input_context, NULL)) {
        av_log(ctx->input_context, AV_LOG_WARNING, "Warning: can't load input file info");
    }
    av_dump_format(ctx->input_context, ctx->input_stream_index, ctx->input_file, 0);

    ret = setup_input_stream(ctx);
    if (ret) {
        exit(ret);
    }

    ctx->input_stream = ctx->input_context->streams[ctx->input_stream_index];
    // Seek to requested position in input file
    av_log(ctx->input_context, AV_LOG_INFO, "Seeking to DTS %ld\n", options_msg->start_dts);
    av_seek_frame(ctx->input_context, ctx->input_stream_index, options_msg->start_dts, AVSEEK_FLAG_FRAME | AVSEEK_FLAG_BACKWARD);

    // Set up output context
    ctx->output_context = NULL;
    avformat_alloc_output_context2(&ctx->output_context, NULL, "mp4", NULL);

    ret = setup_output_stream(ctx);
    if (ret) {
        av_log(NULL, AV_LOG_FATAL, "Error in setup_output_stream\n");
        exit(ret);
    }
    av_dump_format(ctx->output_context, 0, ctx->output_dir, 1);

    ctx->current_segment_index = -1;  // init segment

    ffchunk_start_segment(ctx->output_context, ctx->output_dir, ctx->current_segment_index);

    AVDictionary *options = NULL;
    //av_dict_set(&options, "fflags", "-autobsf", 0);
    // frag_discont makes the value of baseMediaDecodeTime in tfdt be correct for sessions not starting at 0
    av_dict_set(&options, "movflags", "frag_custom+dash+delay_moov+frag_discont", 0);
    av_dict_set(&options, "strict", "-2", 0);
    //// fMP4 fragment index (moof.mfhd.sequence_number) starts at 1
    //// See ISO/IEC 14496-12:2015 Section 8.8.5.1
    av_dict_set_int(&options, "fragment_index", options_msg->segment_start_index+1, 0);
    ret = avformat_write_header(ctx->output_context, &options);
    if (ret < 0) {
        av_log(ctx->output_context, AV_LOG_FATAL, "Error occurred when opening output file\n");
        exit(EXIT_FAILURE);
    }

    // End init segment
    ffchunk_end_segment(ctx->output_context, ctx->output_dir, ctx->current_segment_index, 1);

    ctx->current_segment_index = options_msg->segment_start_index;

    ffchunk_start_segment(ctx->output_context, ctx->output_dir, ctx->current_segment_index);
    ctx->keyframe_required = 1;

    av_dump_format(ctx->input_context, ctx->input_stream_index, ctx->input_file, 0);

    AVPacket pkt;

    // Required when spliting by split points
    int next_split_index = 0;

    // Required when splitting by average segment duration
    int64_t last_split_dts;
    int64_t segment_duration_deviation = 0;
    ctx->first_packet = 1;

    // Number of segments (including the currently open)
    int nb_segments = 1;

    while (av_read_frame(ctx->input_context, &pkt) >= 0) {
        if (pkt.stream_index != ctx->input_stream_index) {
            continue;
        }

        if (ctx->first_packet && pkt.dts != AV_NOPTS_VALUE) {
            ctx->actual_start_dts = pkt.dts;
            ctx->first_packet = 0;
            last_split_dts = ctx->actual_start_dts;
            segment_duration_deviation = ctx->actual_start_dts - options_msg->start_dts;
        }
        if (pkt.dts >= options_msg->end_dts) {
            if (!options_msg->continue_until_keyframe || (pkt.flags & AV_PKT_FLAG_KEY)) {
                break;
            }
        }

        if (options_msg->average_segment_duration) {
            int64_t next_split_dts = last_split_dts + options_msg->average_segment_duration - segment_duration_deviation;
            // When transcoding we also always stop at a keyframe because that's where av_seek_frame will
            // seek to on the next run.
            if ((pkt.flags & AV_PKT_FLAG_KEY)
                && pkt.dts >= next_split_dts
                && (nb_segments < options_msg->nb_segments)) {

                ffchunk_end_segment(ctx->output_context, ctx->output_dir, ctx->current_segment_index, 1);
                ctx->current_segment_index++;
                segment_duration_deviation += pkt.dts - last_split_dts - options_msg->average_segment_duration;
                last_split_dts = pkt.dts;

                ffchunk_start_segment(ctx->output_context, ctx->output_dir, ctx->current_segment_index);
                nb_segments++;
                ctx->keyframe_required = 1;
            }
        } else {
            // Splitting should happen by given splits.
            if (next_split_index < options_msg->n_split_dts && pkt.dts >= options_msg->split_dts[next_split_index]) {
                ffchunk_end_segment(ctx->output_context, ctx->output_dir, ctx->current_segment_index, 1);
                ctx->current_segment_index++;
                next_split_index++;

                ffchunk_start_segment(ctx->output_context, ctx->output_dir, ctx->current_segment_index);
                nb_segments++;
                ctx->keyframe_required = 1;
            }
        }

        process_packet(ctx, &pkt);
    }

    // Fill up with empty segments
    while (nb_segments < options_msg->nb_segments) {
        ffchunk_end_segment(ctx->output_context, ctx->output_dir, ctx->current_segment_index, 1);
        ctx->current_segment_index++;
        ffchunk_start_segment(ctx->output_context, ctx->output_dir, ctx->current_segment_index);
        nb_segments++;
    }

    av_write_trailer(ctx->output_context);
    ffchunk_end_segment(ctx->output_context, ctx->output_dir, ctx->current_segment_index, 0);

    av_dump_format(ctx->output_context, 0, ctx->output_dir, 1);

    av_free(ctx->output_context);

    avformat_close_input(&ctx->input_context);

    free(ctx);

    return 0;
}

#endif //FFCHUNK_COMMON_H
