cmake_minimum_required(VERSION 3.5)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")
find_package(PkgConfig REQUIRED)

project(ffchunk)

find_package(FFmpeg REQUIRED)

pkg_search_module(PROTOBUF REQUIRED libprotobuf-c)

#set(FFMPEG_INCLUDE_DIRS "/home/leon/Development/ffmpeg")
#set(FFMPEG_LIBRARIES
#    "/home/leon/Development/ffmpeg/libavformat/libavformat.a"
#    "/home/leon/Development/ffmpeg/libavcodec/libavcodec.a"
#    "/home/leon/Development/ffmpeg/libavutil/libavutil.a")

add_executable(ffchunk_transmux ffchunk_transmux.c ffchunk_options.pb-c.c)
target_include_directories(ffchunk_transmux PUBLIC ${FFMPEG_INCLUDE_DIRS} ${PROTOBUF_INCLUDE_DIRS})
target_link_libraries(ffchunk_transmux ${FFMPEG_LIBRARIES} ${PROTOBUF_LIBRARIES})

add_executable(ffchunk_transcode_video ffchunk_transcode_video.c ffchunk_options.pb-c.c)
target_include_directories(ffchunk_transcode_video PUBLIC ${FFMPEG_INCLUDE_DIRS} ${PROTOBUF_INCLUDE_DIRS})
target_link_libraries(ffchunk_transcode_video ${FFMPEG_LIBRARIES} ${PROTOBUF_LIBRARIES})

add_executable(ffchunk_transcode_audio ffchunk_transcode_audio.c ffchunk_options.pb-c.c)
target_include_directories(ffchunk_transcode_audio PUBLIC ${FFMPEG_INCLUDE_DIRS} ${SWRESAMPLE_INCLUDE_DIRS} ${PROTOBUF_INCLUDE_DIRS})
target_link_libraries(ffchunk_transcode_audio ${FFMPEG_LIBRARIES} ${SWRESAMPLE_LIBRARIES} ${PROTOBUF_LIBRARIES})

install(TARGETS ffchunk_transmux ffchunk_transcode_video ffchunk_transcode_audio DESTINATION bin)
