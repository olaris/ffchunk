#ifndef FFCHUNK_SEGMENT_H
#define FFCHUNK_SEGMENT_H

#define FFCHUNK_SEGMENT_FILENAME_MAX_SIZE 1024

#include <libavformat/avformat.h>

#include "util.h"

char* ffchunk_alloc_segment_filename(char* output_dir, int segment_index, int tmp) {
    char* segment_filename = malloc(FFCHUNK_SEGMENT_FILENAME_MAX_SIZE*sizeof(char));

    char* tmp_suffix = tmp ? ".tmp" : "";
    if (segment_index < 0) {
        snprintf(segment_filename, FFCHUNK_SEGMENT_FILENAME_MAX_SIZE,
                 "%s/init.mp4%s", output_dir, tmp_suffix);
    } else {
        snprintf(segment_filename, FFCHUNK_SEGMENT_FILENAME_MAX_SIZE,
                 "%s/stream0_%04d.m4s%s", output_dir, segment_index, tmp_suffix);
    }
    return segment_filename;
}

void ffchunk_end_segment(AVFormatContext *output_context, char *output_dir, int segment_index, int write_null_frame) {
    if (write_null_frame) {
        av_write_frame(output_context, NULL);
    }
    avio_close(output_context->pb);

    void *tmp_segment_filename = ffchunk_alloc_segment_filename(output_dir, segment_index, 1);
    void *segment_filename = ffchunk_alloc_segment_filename(output_dir, segment_index, 0);
    av_log(output_context, AV_LOG_INFO, "Ending segment %s and moving to %s\n", tmp_segment_filename, segment_filename);
    rename(tmp_segment_filename, segment_filename);
    free(segment_filename);
}

void ffchunk_start_segment(AVFormatContext* output_context, char* output_dir, int segment_index) {
    char* tmp_segment_filename = ffchunk_alloc_segment_filename(output_dir, segment_index, 1);

    avio_open(&output_context->pb, tmp_segment_filename, AVIO_FLAG_WRITE);
    av_log(output_context, AV_LOG_INFO, "Starting segment %s\n", tmp_segment_filename);
}


#endif //FFCHUNK_SEGMENT_H
