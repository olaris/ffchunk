#include <libavutil/audio_fifo.h>
#include <libavutil/avassert.h>
#include <libswresample/swresample.h>

#include "util.h"
#include "common.h"

#define OUTPUT_SAMPLE_FMT AV_SAMPLE_FMT_FLTP
#define OUTPUT_CHANNELS 2

/* A lot of the code in this file is copied from ffmpeg doc/examples/transcode_aac.c */

typedef struct {
    AVAudioFifo *fifo;
    SwrContext *resample_context;

    int first_frame;
    int64_t frame_pts;

} TranscodeAudioContext;


int init(Context* ctx) {
    ctx->priv = malloc(sizeof(TranscodeAudioContext));
    return 0;
}

int setup_input_stream(Context* ctx) {
    int ret;
    TranscodeAudioContext* priv_ctx = (TranscodeAudioContext*) ctx->priv;

    ctx->input_stream = ctx->input_context->streams[ctx->input_stream_index];
    ret = avcodec_open2(ctx->input_stream->codec,
                        avcodec_find_decoder(ctx->input_stream->codec->codec_id), NULL);
    if (ret < 0) {
        av_log(ctx->input_context, AV_LOG_FATAL, "Failed to open decoder for stream #%u\n", ctx->input_stream_index);
        exit(EXIT_FAILURE);
    }

    priv_ctx->first_frame = 1;

    return 0;
}

/**
 * Initialize the audio resampler based on the input and output codec settings.
 * If the input and output sample formats differ, a conversion is required
 * libswresample takes care of this, but requires initialization.
 * @param      input_codec_context  Codec context of the input file
 * @param      output_codec_context Codec context of the output file
 * @param[out] resample_context     Resample context for the required conversion
 * @return Error code (0 if successful)
 */
static int init_resampler(AVCodecContext *input_codec_context,
                          AVCodecContext *output_codec_context,
                          SwrContext **resample_context)
{
    int error;

    /*
     * Create a resampler context for the conversion.
     * Set the conversion parameters.
     * Default channel layouts based on the number of channels
     * are assumed for simplicity (they are sometimes not detected
     * properly by the demuxer and/or decoder).
     */
    *resample_context = swr_alloc_set_opts(NULL,
                                           av_get_default_channel_layout(output_codec_context->channels),
                                           output_codec_context->sample_fmt,
                                           output_codec_context->sample_rate,
                                           av_get_default_channel_layout(input_codec_context->channels),
                                           input_codec_context->sample_fmt,
                                           input_codec_context->sample_rate,
                                           0, NULL);
    if (!*resample_context) {
        fprintf(stderr, "Could not allocate resample context\n");
        return AVERROR(ENOMEM);
    }
    /*
    * Perform a sanity check so that the number of converted samples is
    * not greater than the number of samples to be converted.
    * If the sample rates differ, this case has to be handled differently
    */
    av_assert0(output_codec_context->sample_rate == input_codec_context->sample_rate);

    /* Open the resampler with the specified parameters. */
    if ((error = swr_init(*resample_context)) < 0) {
        fprintf(stderr, "Could not open resample context\n");
        swr_free(resample_context);
        return error;
    }
    return 0;
}

int setup_output_stream(Context* ctx) {
    int ret;
    TranscodeAudioContext* priv_ctx = (TranscodeAudioContext*) ctx->priv;

    AVCodec* output_codec = avcodec_find_encoder(AV_CODEC_ID_AAC);

    ctx->output_stream = avformat_new_stream(ctx->output_context, output_codec);
    if (!ctx->output_stream) {
        av_log(ctx->output_context, AV_LOG_FATAL, "Failed to allocate output stream\n");
        return EXIT_FAILURE;
    }

    ctx->output_stream->codec->sample_rate = ctx->input_stream->codec->sample_rate;
    ctx->output_stream->time_base.den = ctx->input_stream->codec->sample_rate;
    ctx->output_stream->time_base.num = 1;

    ctx->output_stream->codec->channel_layout = av_get_default_channel_layout(OUTPUT_CHANNELS);
    ctx->output_stream->codec->channels = OUTPUT_CHANNELS;
    ctx->output_stream->codec->sample_fmt = OUTPUT_SAMPLE_FMT;
    ctx->output_stream->codec->bit_rate = 128000;
    // -strict -2 for old libav versions
    ctx->output_stream->codec->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;

    ret = avcodec_open2(ctx->output_stream->codec, NULL, NULL);
    if (ret < 0) {
        av_log(ctx->output_context, AV_LOG_ERROR, "Cannot open audio encoder for stream\n");
        return ret;
    }

    priv_ctx->fifo = av_audio_fifo_alloc(OUTPUT_SAMPLE_FMT, OUTPUT_CHANNELS, 1);
    if (!priv_ctx->fifo) {
        av_log(NULL, AV_LOG_ERROR, "Failed to allocate audio FIFO\n");
        return EXIT_FAILURE;
    }

    init_resampler(ctx->input_stream->codec, ctx->output_stream->codec, &priv_ctx->resample_context);

    return 0;
}


/**
 * Initialize a temporary storage for the specified number of audio samples.
 * The conversion requires temporary storage due to the different format.
 * The number of audio samples to be allocated is specified in frame_size.
 * @param[out] converted_input_samples Array of converted samples. The
 *                                     dimensions are reference, channel
 *                                     (for multi-channel audio), sample.
 * @param      output_codec_context    Codec context of the output file
 * @param      frame_size              Number of samples to be converted in
 *                                     each round
 * @return Error code (0 if successful)
 */
static int init_converted_samples(uint8_t ***converted_input_samples,
                                  AVCodecContext *output_codec_context,
                                  int frame_size)
{
    int error;

    /* Allocate as many pointers as there are audio channels.
     * Each pointer will later point to the audio samples of the corresponding
     * channels (although it may be NULL for interleaved formats).
     */
    if (!(*converted_input_samples = calloc(output_codec_context->channels,
                                            sizeof(**converted_input_samples)))) {
        fprintf(stderr, "Could not allocate converted input sample pointers\n");
        return AVERROR(ENOMEM);
    }

    /* Allocate memory for the samples of all channels in one consecutive
     * block for convenience. */
    if ((error = av_samples_alloc(*converted_input_samples, NULL,
                                  output_codec_context->channels,
                                  frame_size,
                                  output_codec_context->sample_fmt, 0)) < 0) {
        fprintf(stderr,
                "Could not allocate converted input samples (error '%s')\n",
                av_err2str(error));
        av_freep(&(*converted_input_samples)[0]);
        free(*converted_input_samples);
        return error;
    }
    return 0;
}

/**
 * Convert the input audio samples into the output sample format.
 * The conversion happens on a per-frame basis, the size of which is
 * specified by frame_size.
 * @param      input_data       Samples to be decoded. The dimensions are
 *                              channel (for multi-channel audio), sample.
 * @param[out] converted_data   Converted samples. The dimensions are channel
 *                              (for multi-channel audio), sample.
 * @param      frame_size       Number of samples to be converted
 * @param      resample_context Resample context for the conversion
 * @return Error code (0 if successful)
 */
static int convert_samples(const uint8_t **input_data,
                           uint8_t **converted_data, const int frame_size,
                           SwrContext *resample_context)
{
    int error;

    /* Convert the samples using the resampler. */
    if ((error = swr_convert(resample_context,
                             converted_data, frame_size,
                             input_data    , frame_size)) < 0) {
        fprintf(stderr, "Could not convert input samples (error '%s')\n",
                av_err2str(error));
        return error;
    }

    return 0;
}

/**
 * Add converted input audio samples to the FIFO buffer for later processing.
 * @param fifo                    Buffer to add the samples to
 * @param converted_input_samples Samples to be added. The dimensions are channel
 *                                (for multi-channel audio), sample.
 * @param frame_size              Number of samples to be converted
 * @return Error code (0 if successful)
 */
static int add_samples_to_fifo(AVAudioFifo *fifo,
                               uint8_t **converted_input_samples,
                               const int frame_size)
{
    int error;

    /* Make the FIFO as large as it needs to be to hold both,
     * the old and the new samples. */
    if ((error = av_audio_fifo_realloc(fifo, av_audio_fifo_size(fifo) + frame_size)) < 0) {
        fprintf(stderr, "Could not reallocate FIFO\n");
        return error;
    }

    /* Store the new samples in the FIFO buffer. */
    if (av_audio_fifo_write(fifo, (void **)converted_input_samples,
                            frame_size) < frame_size) {
        fprintf(stderr, "Could not write data to FIFO\n");
        return AVERROR_EXIT;
    }
    return 0;
}

static int encode_frame_from_fifo(Context* ctx, AVFrame* output_frame) {
    TranscodeAudioContext* priv_ctx = (TranscodeAudioContext*) ctx->priv;

    const int frame_size = FFMIN(av_audio_fifo_size(priv_ctx->fifo),
                                 ctx->output_stream->codec->frame_size);

    output_frame->nb_samples = frame_size;
    output_frame->channel_layout = ctx->output_stream->codec->channel_layout;
    output_frame->format = ctx->output_stream->codec->sample_fmt;
    output_frame->sample_rate = ctx->output_stream->codec->sample_rate;

    av_frame_get_buffer(output_frame, 0);
    av_audio_fifo_read(priv_ctx->fifo, (void **)output_frame->data, frame_size);

    output_frame->pts = priv_ctx->frame_pts;
    priv_ctx->frame_pts += output_frame->nb_samples;
}

int process_packet(Context* ctx, AVPacket* pkt) {
    int err;
    TranscodeAudioContext* priv_ctx = (TranscodeAudioContext*) ctx->priv;

    /* Temporary storage for the converted input samples. */
    uint8_t **converted_input_samples = NULL;

    log_packet(ctx->input_context, pkt, "in");

    av_packet_rescale_ts(pkt, ctx->input_stream->time_base, ctx->input_stream->codec->time_base);

    AVFrame *input_frame = av_frame_alloc();
    int got_frame = 0;
    err = avcodec_decode_audio4(ctx->input_stream->codec, input_frame, &got_frame, pkt);
    if (err < 0 || !got_frame) {
        av_log(ctx->input_context, AV_LOG_ERROR,
                "Failed to decode audio frame from packet with DTS %ld: %d\n", pkt->dts, err);
        av_frame_free(&input_frame);
        return err;
    }

    // Use the PTS of the first frame as a base, output frames will have their PTS based on how many samples
    // were written.
    if (priv_ctx->first_frame) {
        priv_ctx->frame_pts = input_frame->pts;
        priv_ctx->first_frame = 0;
    }

    /* Initialize the temporary storage for the converted input samples. */
    init_converted_samples(&converted_input_samples, ctx->output_stream->codec, input_frame->nb_samples);
    /* Convert the input samples to the desired output sample format.
     * This requires a temporary storage provided by converted_input_samples. */
    convert_samples(
            (const uint8_t **) input_frame->extended_data, converted_input_samples,
            input_frame->nb_samples, priv_ctx->resample_context);
    /* Add the converted input samples to the FIFO buffer for later processing. */
    add_samples_to_fifo(priv_ctx->fifo, converted_input_samples, input_frame->nb_samples);

    // TODO(Leon Handreke): Read the last couple of samples from the queue for the last packet.
    while (av_audio_fifo_size(priv_ctx->fifo) >= ctx->output_stream->codec->frame_size) {
        AVFrame *output_frame = av_frame_alloc();
        encode_frame_from_fifo(ctx, output_frame);

        AVPacket output_packet;
        output_packet.size = 0;
        output_packet.data = NULL;
        av_init_packet(&output_packet);
        int got_packet;
        err = avcodec_encode_audio2(ctx->output_stream->codec, &output_packet, output_frame, &got_packet);
        if (err < 0 || !got_packet) {
            av_log(ctx->input_context, AV_LOG_ERROR,
                   "Failed to encode audio frame for frame with PTS %ld: %d\n", output_frame->pts, err);
            av_frame_free(&output_frame);
            av_free_packet(&output_packet);
            return err;
        }
        av_frame_free(&output_frame);

        av_packet_rescale_ts(&output_packet, ctx->output_stream->codec->time_base, ctx->output_stream->time_base);
        output_packet.stream_index = 0;
        output_packet.pos = -1;
        //log_packet(ctx->output_context, output_packet, "out");

        av_interleaved_write_frame(ctx->output_context, &output_packet);
        av_free_packet(&output_packet);
    }
}

int main(int argc, char **argv) {
    return run(argc, argv, &init, &setup_input_stream, &setup_output_stream, &process_packet);
}
