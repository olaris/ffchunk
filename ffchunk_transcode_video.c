#include "util.h"
#include "common.h"

int setup_input_stream(Context* ctx) {
    int ret;

    ctx->input_stream = ctx->input_context->streams[ctx->input_stream_index];
    ret = avcodec_open2(ctx->input_stream->codec,
                        avcodec_find_decoder(ctx->input_stream->codec->codec_id), NULL);
    if (ret < 0) {
        av_log(ctx->input_context, AV_LOG_FATAL, "Failed to open decoder for stream #%u\n", ctx->input_stream_index);
        exit(EXIT_FAILURE);
    }

    if (ctx->input_stream->codec->codec_type != AVMEDIA_TYPE_VIDEO
        && ctx->input_stream->codec->codec_type != AVMEDIA_TYPE_AUDIO) {
        av_log(ctx->input_context, AV_LOG_FATAL, "Not an audio or video stream\n");
        exit(EXIT_FAILURE);
    }

    return 0;
}

int setup_output_stream(Context* ctx) {
    int ret;

    AVCodec* output_codec = avcodec_find_encoder(AV_CODEC_ID_H264);

    ctx->output_stream = avformat_new_stream(ctx->output_context, output_codec);
    if (!ctx->output_stream) {
        av_log(ctx->output_context, AV_LOG_FATAL, "Failed to allocate output stream\n");
        return EXIT_FAILURE;
    }

    ctx->output_stream->codec->height = ctx->input_stream->codec->height;
    ctx->output_stream->codec->width = ctx->input_stream->codec->width;
    ctx->output_stream->codec->pix_fmt = AV_PIX_FMT_YUV420P;
    ctx->output_stream->codec->time_base = ctx->input_stream->codec->time_base;
    ctx->output_stream->codec->level = 31;
    ret = av_opt_set(ctx->output_stream->codec->priv_data, "profile", "main", 0);
    if (ret < 0) {
        av_log(ctx->output_context, AV_LOG_ERROR, "Could not set option profile=main: %d\n", ret);
        exit(EXIT_FAILURE);
    }
    ret = av_opt_set(ctx->output_stream->codec->priv_data, "preset", "veryfast", 0);
    if (ret < 0) {
        av_log(ctx->output_context, AV_LOG_ERROR, "Could not set option preset=veryfast: %d\n", ret);
        exit(EXIT_FAILURE);
    }
    ret = av_opt_set(ctx->output_stream->codec->priv_data, "crf", "12", 0);
    if (ret < 0) {
        av_log(ctx->output_context, AV_LOG_ERROR, "Could not set option crf=12: %d\n", ret);
        exit(EXIT_FAILURE);
    }
    // 1000k
    ctx->output_stream->codec->bit_rate = 1000000;

    ret = avcodec_open2(ctx->output_stream->codec, NULL, NULL);
    if (ret < 0) {
        av_log(ctx->output_context, AV_LOG_ERROR,
                "Cannot open video encoder for stream of type %d\n",
                ctx->output_stream->codec->codec_type);
        return ret;
    }

    return 0;
}

int process_packet(Context* ctx, AVPacket* pkt) {
    int err, ret;

    //process_packet_transmux(ctx, &pkt);
    av_packet_rescale_ts(pkt, ctx->input_stream->time_base, ctx->input_stream->codec->time_base);

    AVFrame *frame = av_frame_alloc();

    int got_picture = 0;
    err = avcodec_decode_video2(ctx->input_stream->codec, frame, &got_picture, pkt);
    if (err < 0) {
        av_log(ctx->input_context, AV_LOG_ERROR, "Failed to decode packet with DTS %ld", pkt->dts);
        ret = err;
        goto cleanup;
    }
    if (!got_picture) {
        av_log(ctx->input_context, AV_LOG_WARNING, "No video frame in packet with DTS %ld", pkt->dts);
        ret = 0;
        goto cleanup;
    }

    if (ctx->keyframe_required) {
        frame->pict_type = AV_PICTURE_TYPE_I;
        frame->key_frame = 1;
        ctx->keyframe_required = 0;
    }
    frame->pts = av_rescale_q(frame->pts, ctx->input_stream->codec->time_base, ctx->output_stream->codec->time_base);

    AVPacket enc_pkt;
    enc_pkt.size = 0;
    enc_pkt.data = NULL;
    av_init_packet(&enc_pkt);

    int got_packet = 0;
    err = avcodec_encode_video2(ctx->output_stream->codec, &enc_pkt, frame, &got_packet);
    if (err < 0 || !got_packet) {
        av_log(ctx->input_context, AV_LOG_ERROR, "Failed to encode frame with PTS %ld. err=%d got_packet=%d", frame->pts, err, got_packet);
        ret = err;
        av_free_packet(&enc_pkt);
        goto cleanup;
    }

    av_packet_rescale_ts(&enc_pkt, ctx->output_stream->codec->time_base, ctx->output_stream->time_base);
    enc_pkt.stream_index = 0;
    enc_pkt.pos = -1;
    log_packet(ctx->output_context, &enc_pkt, "out");

    av_interleaved_write_frame(ctx->output_context, &enc_pkt);
    av_free_packet(&enc_pkt);

cleanup:
    av_frame_free(&frame);
    return ret;
}

int main(int argc, char **argv) {
    return run(argc, argv, NULL, &setup_input_stream, &setup_output_stream, &process_packet);
}
