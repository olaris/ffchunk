#include <libavutil/timestamp.h>
#include <libavutil/opt.h>

#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>

#include "common.h"

int setup_input_stream(Context* ctx) {
    return 0;
}

int setup_output_stream(Context* ctx) {
    int ret;

    ctx->output_stream = avformat_new_stream(ctx->output_context, ctx->input_stream->codec->codec);
    ret = avcodec_copy_context(ctx->output_stream->codec, ctx->input_stream->codec);
    if (ret < 0) {
        av_log(NULL, AV_LOG_FATAL, "Failed to copy context from input to output stream codec context\n");
        return EXIT_FAILURE;
    }
}

int process_packet(Context* ctx, AVPacket* pkt) {
    log_packet(ctx->input_context, pkt, "in");
    av_packet_rescale_ts(pkt, ctx->input_stream->time_base, ctx->output_stream->time_base);
    pkt->stream_index = 0;
    log_packet(ctx->input_context, pkt, "out");

    av_interleaved_write_frame(ctx->output_context, pkt);
}

int main(int argc, char **argv) {
    return run(argc, argv, NULL, &setup_input_stream, &setup_output_stream, &process_packet);
}
